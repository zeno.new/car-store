package com.webank.blockchain.lagcredit.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;

import com.webank.blockchain.lagcredit.BaseTest;
import com.webank.blockchain.lagcredit.contracts.CarMarket;
import com.webank.blockchain.lagcredit.contracts.LAGCredit;

import lombok.extern.slf4j.Slf4j;
import mo.com.carmarket.Car;
import mo.com.carmarket.User;

@Slf4j
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CarMarketServiceTest extends BaseTest {
	@Autowired
	private CarMarketService carMarketService;

	static private String carMarketAddr = "";
	static private User buyer;
	static private User seller;

	@Test
	public void testAdeploy() {
		log.info("----test deploy---");
		CarMarket carMarket = carMarketService.deploy();
		log.info("deploy LAGCredit address : {}", carMarket);
		carMarketAddr = carMarket.getContractAddress();
	}

	@Test
	public void testBload() {
		log.info("----test load---");
		CarMarket carMarket = carMarketService.load(carMarketAddr);
		log.info("load LAGCredit address : {}", carMarket.getContractAddress());
	}

	@Test
	public void testCCreateSeller() throws Exception {
		log.info("--- create user");
		seller = carMarketService.createUser(carMarketAddr );

		log.info("--- seller address {}", seller.getAddress());
	}

	@Test
	public void testCInsertCar() throws Exception {
		log.info("--- insert car");
		boolean result = carMarketService.insertCar(carMarketAddr, seller, "1234", "BMW", 100);

		assertTrue(result);
	}

	@Test
	public void testDCarList() throws Exception {
		long size = carMarketService.getListSize(carMarketAddr);
		log.info("car size : {}", size);
		assertEquals(1l, size);
	}

	@Test
	public void testEGetCar() throws Exception {
		log.info("--- get car");
		Car car = carMarketService.getCar(carMarketAddr, 0);
		assertEquals("1234", car.getCarNo());
	}

	@Test
	public void testFCreatebuyer() throws Exception {
		log.info("--- create user");
		buyer = carMarketService.createUser(carMarketAddr);

		log.info("--- buyer address {}", buyer.getAddress());
	}

//	@Test
//	public void testGSellerCannotBuyOwnedCar() {
//		log.info("-- purchase car");
//
//		try {
//			carMarketService.purchase(carMarketAddr, seller.getAddress(), 0);
//			assertTrue(false);
//		} catch (Exception e) {
//
//		}
//	}

	@Test
	public void testHPurchaseCar() throws Exception {
		log.info("-- purchase car");

		carMarketService.purchase(carMarketAddr, buyer, 0);

	}
	
}
