pragma solidity ^0.4.24;
pragma experimental ABIEncoderV2;

contract CarMarket{

    uint car_id_sequence = 0;//carId sequence
    int public salesCount;  //售賣次數
    int256 totalPrice; //總售賣價格
    //bank
    mapping (address => int) private balances;
    
    struct Car{
        uint carId;
        string carNo;
        string carModel;
        int price;
        address owner_address;
        bool solded;//false為下架
    }
    
    Car[] carlist;
    
    address private admin; //管理員的地址
    
    //建立用戶及始化資金
    function createUser() public{
        //usersTable.insert(userName, 1000);
        balances[msg.sender] = 1000;
    }
    
    function showBalance() public returns (int){
        return balances[msg.sender];
    }
     
    function getListSize() constant returns (uint256){
       return carlist.length;
    }
    
	//取得全部車
    function getCarList() public constant returns (uint[] carId,string[] carNo, string[] carModel, 
    int[] price,bool[] solded,address[] owner_address){
        return _getCarList();
    }
    
	//取得全部車內部邏輯
    function _getCarList() internal constant returns (uint[] carId,string[] carNo, string[] carModel, 
    int[] price,bool[] solded,address[] owner_address){
        carId = new uint[](getListSize());
        carNo = new string[](getListSize());
        carModel = new string[](getListSize());
        price = new int[](getListSize());
        solded = new bool[](getListSize());
        owner_address = new address[](getListSize());
        
        if(getListSize() > 0){
            for(uint i=0; i < getListSize(); i++){
                Car memory c = carlist[i];
                carId[i] = c.carId;
                carNo[i] = c.carNo;
                carModel[i] = c.carModel;
                price[i] =c.price ;
                owner_address[i] = c.owner_address;
                solded[i] = c.solded;
            }
        }
        
    }
    
     //新增汽車
    function insertCar(string carNo, string carModel, int price) public {
        Car memory c;
        c.carId = car_id_sequence++;
        c.carNo = carNo;
        c.carModel = carModel;
        c.price =price ;
        c.owner_address = msg.sender;
        c.solded = false;
        carlist.push(c);
    }
    
    function getCar(uint carId) public constant returns (string carNo,
        string carModel,int price,address owner_address,bool solded){
        for(uint256 i=0; i< carlist.length; i++){
            Car memory c = carlist[i];
            if( c.carId == carId){
                carNo = c.carNo;
                carModel = c.carModel;
                price =c.price ;
                owner_address = c.owner_address;
                solded = c.solded;
                break;
            }
        }
    }
    
    //查找汽車
    function searchCar(uint carId) internal returns (Car) {
        for(uint256 i=0; i< carlist.length; i++){
            Car memory c = carlist[i];
            if( c.carId == carId){
                return c; 
            }
        }
    }
    
    //外部修改
    function outerModifyCar(uint carId,string carNo, string carModel, int price,bool solded) public {
        modifyCar(carId,carNo,carModel,price,solded,msg.sender);
    }

    //內部修改
    function innerModifyCar (uint carId,string carNo, string carModel, int price,bool solded,address owner_address) public {
        modifyCar(carId,carNo,carModel,price,solded,owner_address);
    }
    
    //修改汽車
    function modifyCar(uint carId,string carNo, string carModel, int price,bool solded,address owner_address) private {
        Car memory c= searchCar(carId);
        c.carNo = carNo;
        c.carModel = carModel;
        c.price =price ;
        c.solded = solded;
        c.owner_address = owner_address;
        carlist[carId] = c;
    }
    
    function purchaseCar(uint carId) public {
        _purchaseCar(msg.sender, carId);
    }

    //買車的處理邏輯
    function _purchaseCar(address buyer, uint carId) internal{
        Car memory c = searchCar(carId);
        //判斷是否已上架
        require(c.solded == false);
        //自已不可以同自己交易
        require(buyer != c.owner_address);
        //判斷餘額是否足夠購買
        require(balances[buyer] >= c.price);
        
        balances[buyer] -= c.price;//比錢
        balances[c.owner_address] += c.price;//收錢
        
        c.owner_address = buyer;
        c.solded = true;
        carlist[c.carId] = c;
        
        //記錄統計資料
        salesCount++;
        totalPrice+=c.price;
    }
    
    //平均售賣價格
    function avgPrice() public constant returns(int){
        return totalPrice / salesCount;
    }
    
	//售賣次數
    function getSalesCount() public constant returns(int){
        return salesCount;
    }
    
	//總售賣價格
    function getTotalPrice ()public constant returns(int){
        return totalPrice;
    }
    
    //重新上架
    function reActiveCar(uint carId, int price)public{
        _reActiveCar(msg.sender, carId, price);
    }
    
    //重新上架的處理邏輯
    function _reActiveCar(address buyer, uint carId, int price)internal{
        Car memory c = searchCar(carId);
        //判斷是否已上架
        require(c.solded == true);
        //自已不可以同自己交易
        require(buyer == c.owner_address);
        
        c.price = price;
        c.solded = false;
        carlist[c.carId] = c;
    }
    
}
