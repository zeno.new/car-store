package com.webank.blockchain.lagcredit.controllers;

import io.swagger.annotations.Api;

import java.util.List;

import javax.ws.rs.PathParam;

import org.fisco.bcos.web3j.protocol.core.methods.response.TransactionReceipt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import com.webank.blockchain.lagcredit.service.CarMarketService;

import lombok.extern.slf4j.Slf4j;
import mo.com.carmarket.Car;
import mo.com.carmarket.User;

@Api(value = "Sample", description = "test des", produces = MediaType.ALL_VALUE)
@Slf4j
@RestController
public class CarMarketApplicationController {

	@Autowired
	private CarMarketService carMarketService;
	
	@Value("${address-config.carMarket-contract-address}")
	public String carMarketContractAddress;
	
	@GetMapping("/create-user")
	public User createUser() throws Exception{
		System.out.println("rest api to call createUser");
		return carMarketService.createUser(carMarketContractAddress);
	}
	
	@GetMapping("/post-car/{privateKey}/{carNo}/{carModel}/{price}")
	public boolean postCar(@PathVariable(value="privateKey")String privateKey,@PathVariable(value ="carNo") String carNo,
			@PathVariable(value ="carModel") String carModel,@PathVariable(value="price") Integer price) throws Exception {
		User seller = new User("", privateKey, "");
		return carMarketService.insertCar(carMarketContractAddress, seller, carNo, carModel, price);
	}

	@GetMapping("/get-car/{carId}")
	public Car getCar(@PathVariable(value="carId")int carId) throws Exception {
		Car car = carMarketService.getCar(carMarketContractAddress, carId);
		return car;
	}
	@GetMapping("/get-cars")
	public List<Car> getCarList() throws Exception{
		return carMarketService.getCarList(carMarketContractAddress);
	}
	
	@GetMapping("/purchase/{privateKey}/{carId}")
	public boolean purchase(@PathVariable(value="privateKey")String privateKey,@PathVariable(value="carId") int carId) throws Exception {
		System.out.println("carId : " + carId);
		User buyer = new User("", privateKey, "");
		return carMarketService.purchase(carMarketContractAddress, buyer, carId);
	}
	@GetMapping("/show-balance/{privateKey}")
	public TransactionReceipt showBalance(@PathVariable(value="privateKey")String privateKey) throws Exception {
		User user = new User("", privateKey, "");
		return carMarketService.showBalance(carMarketContractAddress, user);
	}
	
	@GetMapping("/total-price")
	public int getTotalPrice() throws Exception {
		return carMarketService.getTotalPrice(carMarketContractAddress);
	}
	
	@GetMapping("/sales-count")
	public int getSalesCount() throws Exception {
		return carMarketService.getSalesCount(carMarketContractAddress);
	}
	
	@GetMapping("/get-avg-price")
	public int getAvgPrice() throws Exception {
		return carMarketService.getAvgPrice(carMarketContractAddress);
	}
	//restful��蝷箔��
//	@ApiOperation(value = "甈Ｚ�△", notes = "甈Ｚ�△靽⊥",httpMethod = "GET",produces = MediaType.ALL_VALUE)
//	@GetMapping("/hello")
//	@ResponseBody
//	@ApiImplicitParams({
//			@ApiImplicitParam(name = "name",value = "������",required =true,dataType ="String"),
//			@ApiImplicitParam(name = "text",value = "��捆",required =true,dataType ="String")
//	})
//	public String helloRest(@RequestParam String name,
//							@RequestParam String text){
//		System.out.println("hello restful!");
////		HashMap<String,String> result = new HashMap<String,String>();
////		result.put("name", helloService.sayHello(name));
////		result.put("text", text);
////		result.put("regard", "regarding");
//		return "result";
//	}
}
