package com.webank.blockchain.lagcredit.contracts;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import org.fisco.bcos.channel.client.TransactionSucCallback;
import org.fisco.bcos.web3j.abi.TypeReference;
import org.fisco.bcos.web3j.abi.datatypes.Address;
import org.fisco.bcos.web3j.abi.datatypes.Bool;
import org.fisco.bcos.web3j.abi.datatypes.DynamicArray;
import org.fisco.bcos.web3j.abi.datatypes.Function;
import org.fisco.bcos.web3j.abi.datatypes.Type;
import org.fisco.bcos.web3j.abi.datatypes.Utf8String;
import org.fisco.bcos.web3j.abi.datatypes.generated.Int256;
import org.fisco.bcos.web3j.abi.datatypes.generated.Uint256;
import org.fisco.bcos.web3j.crypto.Credentials;
import org.fisco.bcos.web3j.protocol.Web3j;
import org.fisco.bcos.web3j.protocol.core.RemoteCall;
import org.fisco.bcos.web3j.protocol.core.methods.response.TransactionReceipt;
import org.fisco.bcos.web3j.tuples.generated.Tuple5;
import org.fisco.bcos.web3j.tuples.generated.Tuple6;
import org.fisco.bcos.web3j.tx.Contract;
import org.fisco.bcos.web3j.tx.TransactionManager;
import org.fisco.bcos.web3j.tx.gas.ContractGasProvider;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.fisco.bcos.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version none.
 */
@SuppressWarnings("unchecked")
public class CarMarket extends Contract {
    public static String BINARY = "60806040526000805534801561001457600080fd5b50611e35806100246000396000f3006080604052600436106100d0576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff16806325ad91ce146100d55780632d7a6f25146100ec5780632eee4605146101155780634a89a6c81461013e5780635e8be19014610169578063682789a8146101aa57806381b2d07b146101d5578063924f730714610200578063c4912a9114610230578063ce2dc6ca1461025b578063dd8027db14610284578063e0ba3fe5146102af578063ecfab294146102d8578063f475409f14610301575b600080fd5b3480156100e157600080fd5b506100ea61032c565b005b3480156100f857600080fd5b50610113600480360361010e91908101906117a3565b610374565b005b34801561012157600080fd5b5061013c600480360361013791908101906116c0565b61038a565b005b34801561014a57600080fd5b50610153610399565b6040516101609190611be5565b60405180910390f35b34801561017557600080fd5b50610190600480360361018b9190810190611697565b6103a6565b6040516101a1959493929190611b84565b60405180910390f35b3480156101b657600080fd5b506101bf610609565b6040516101cc9190611b69565b60405180910390f35b3480156101e157600080fd5b506101ea61060f565b6040516101f79190611b69565b60405180910390f35b34801561020c57600080fd5b50610215610656565b60405161022796959493929190611ade565b60405180910390f35b34801561023c57600080fd5b5061024561067b565b6040516102529190611b69565b60405180910390f35b34801561026757600080fd5b50610282600480360361027d91908101906116fc565b610692565b005b34801561029057600080fd5b506102996106a7565b6040516102a69190611b69565b60405180910390f35b3480156102bb57600080fd5b506102d660048036036102d19190810190611697565b6106b1565b005b3480156102e457600080fd5b506102ff60048036036102fa9190810190611618565b6106be565b005b34801561030d57600080fd5b50610316610830565b6040516103239190611b69565b60405180910390f35b6103e8600360003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002081905550565b61038286868686868661083a565b505050505050565b610395338383610990565b5050565b6000600480549050905090565b6060806000806000806103b761147e565b600091505b6004805490508210156105fe576004828154811015156103d857fe5b906000526020600020906005020160c0604051908101604052908160008201548152602001600182018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156104955780601f1061046a57610100808354040283529160200191610495565b820191906000526020600020905b81548152906001019060200180831161047857829003601f168201915b50505050508152602001600282018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156105375780601f1061050c57610100808354040283529160200191610537565b820191906000526020600020905b81548152906001019060200180831161051a57829003601f168201915b50505050508152602001600382015481526020016004820160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020016004820160149054906101000a900460ff161515151581525050905087816000015114156105f157806020015196508060400151955080606001519450806080015193508060a0015192506105fe565b81806001019250506103bc565b505091939590929450565b60015481565b6000600360003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002054905090565b606080606080606080610667610af3565b955095509550955095509550909192939495565b600060015460025481151561068c57fe5b05905090565b6106a085858585853361083a565b5050505050565b6000600154905090565b6106bb3382610f8e565b50565b6106c661147e565b600080815480929190600101919050558160000181815250508381602001819052508281604001819052508181606001818152505033816080019073ffffffffffffffffffffffffffffffffffffffff16908173ffffffffffffffffffffffffffffffffffffffff168152505060008160a0019015159081151581525050600481908060018154018082558091505090600182039060005260206000209060050201600090919290919091506000820151816000015560208201518160010190805190602001906107989291906114cd565b5060408201518160020190805190602001906107b59291906114cd565b506060820151816003015560808201518160040160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555060a08201518160040160146101000a81548160ff02191690831515021790555050505050505050565b6000600254905090565b61084261147e565b61084b8761123d565b905085816020018190525084816040018190525083816060018181525050828160a001901515908115158152505081816080019073ffffffffffffffffffffffffffffffffffffffff16908173ffffffffffffffffffffffffffffffffffffffff1681525050806004888154811015156108c157fe5b90600052602060002090600502016000820151816000015560208201518160010190805190602001906108f59291906114cd565b5060408201518160020190805190602001906109129291906114cd565b506060820151816003015560808201518160040160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555060a08201518160040160146101000a81548160ff02191690831515021790555090505050505050505050565b61099861147e565b6109a18361123d565b9050600115158160a0015115151415156109ba57600080fd5b806080015173ffffffffffffffffffffffffffffffffffffffff168473ffffffffffffffffffffffffffffffffffffffff161415156109f857600080fd5b8181606001818152505060008160a00190151590811515815250508060048260000151815481101515610a2757fe5b9060005260206000209060050201600082015181600001556020820151816001019080519060200190610a5b9291906114cd565b506040820151816002019080519060200190610a789291906114cd565b506060820151816003015560808201518160040160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555060a08201518160040160146101000a81548160ff02191690831515021790555090505050505050565b6060806060806060806000610b0661147e565b610b0e610399565b604051908082528060200260200182016040528015610b3c5781602001602082028038833980820191505090505b509750610b47610399565b604051908082528060200260200182016040528015610b7a57816020015b6060815260200190600190039081610b655790505b509650610b85610399565b604051908082528060200260200182016040528015610bb857816020015b6060815260200190600190039081610ba35790505b509550610bc3610399565b604051908082528060200260200182016040528015610bf15781602001602082028038833980820191505090505b509450610bfc610399565b604051908082528060200260200182016040528015610c2a5781602001602082028038833980820191505090505b509350610c35610399565b604051908082528060200260200182016040528015610c635781602001602082028038833980820191505090505b5092506000610c70610399565b1115610f8457600091505b610c83610399565b821015610f8357600482815481101515610c9957fe5b906000526020600020906005020160c0604051908101604052908160008201548152602001600182018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610d565780601f10610d2b57610100808354040283529160200191610d56565b820191906000526020600020905b815481529060010190602001808311610d3957829003601f168201915b50505050508152602001600282018054600181600116156101000203166002900480601f016020809104026020016040519081016040528092919081815260200182805460018160011615610100020316600290048015610df85780601f10610dcd57610100808354040283529160200191610df8565b820191906000526020600020905b815481529060010190602001808311610ddb57829003601f168201915b50505050508152602001600382015481526020016004820160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020016004820160149054906101000a900460ff161515151581525050905080600001518883815181101515610e9157fe5b906020019060200201818152505080602001518783815181101515610eb257fe5b9060200190602002018190525080604001518683815181101515610ed257fe5b9060200190602002018190525080606001518583815181101515610ef257fe5b906020019060200201818152505080608001518383815181101515610f1357fe5b9060200190602002019073ffffffffffffffffffffffffffffffffffffffff16908173ffffffffffffffffffffffffffffffffffffffff16815250508060a001518483815181101515610f6257fe5b90602001906020020190151590811515815250508180600101925050610c7b565b5b5050909192939495565b610f9661147e565b610f9f8261123d565b9050600015158160a001511515141515610fb857600080fd5b806080015173ffffffffffffffffffffffffffffffffffffffff168373ffffffffffffffffffffffffffffffffffffffff1614151515610ff757600080fd5b8060600151600360008573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff168152602001908152602001600020541215151561104957600080fd5b8060600151600360008573ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060008282540392505081905550806060015160036000836080015173ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020019081526020016000206000828254019250508190555082816080019073ffffffffffffffffffffffffffffffffffffffff16908173ffffffffffffffffffffffffffffffffffffffff168152505060018160a0019015159081151581525050806004826000015181548110151561114c57fe5b90600052602060002090600502016000820151816000015560208201518160010190805190602001906111809291906114cd565b50604082015181600201908051906020019061119d9291906114cd565b506060820151816003015560808201518160040160006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555060a08201518160040160146101000a81548160ff0219169083151502179055509050506001600081548092919060010191905055508060600151600260008282540192505081905550505050565b61124561147e565b600061124f61147e565b600091505b6004805490508210156114765760048281548110151561127057fe5b906000526020600020906005020160c0604051908101604052908160008201548152602001600182018054600181600116156101000203166002900480601f01602080910402602001604051908101604052809291908181526020018280546001816001161561010002031660029004801561132d5780601f106113025761010080835404028352916020019161132d565b820191906000526020600020905b81548152906001019060200180831161131057829003601f168201915b50505050508152602001600282018054600181600116156101000203166002900480601f0160208091040260200160405190810160405280929190818152602001828054600181600116156101000203166002900480156113cf5780601f106113a4576101008083540402835291602001916113cf565b820191906000526020600020905b8154815290600101906020018083116113b257829003601f168201915b50505050508152602001600382015481526020016004820160009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff1681526020016004820160149054906101000a900460ff1615151515815250509050838160000151141561146957809250611477565b8180600101925050611254565b5b5050919050565b60c06040519081016040528060008152602001606081526020016060815260200160008152602001600073ffffffffffffffffffffffffffffffffffffffff1681526020016000151581525090565b828054600181600116156101000203166002900490600052602060002090601f016020900481019282601f1061150e57805160ff191683800117855561153c565b8280016001018555821561153c579182015b8281111561153b578251825591602001919060010190611520565b5b509050611549919061154d565b5090565b61156f91905b8082111561156b576000816000905550600101611553565b5090565b90565b600061157e8235611d68565b905092915050565b60006115928235611d88565b905092915050565b60006115a68235611d94565b905092915050565b600082601f83011215156115c157600080fd5b81356115d46115cf82611c2d565b611c00565b915080825260208301602083018583830111156115f057600080fd5b6115fb838284611da8565b50505092915050565b60006116108235611d9e565b905092915050565b60008060006060848603121561162d57600080fd5b600084013567ffffffffffffffff81111561164757600080fd5b611653868287016115ae565b935050602084013567ffffffffffffffff81111561167057600080fd5b61167c868287016115ae565b925050604061168d8682870161159a565b9150509250925092565b6000602082840312156116a957600080fd5b60006116b784828501611604565b91505092915050565b600080604083850312156116d357600080fd5b60006116e185828601611604565b92505060206116f28582860161159a565b9150509250929050565b600080600080600060a0868803121561171457600080fd5b600061172288828901611604565b955050602086013567ffffffffffffffff81111561173f57600080fd5b61174b888289016115ae565b945050604086013567ffffffffffffffff81111561176857600080fd5b611774888289016115ae565b93505060606117858882890161159a565b925050608061179688828901611586565b9150509295509295909350565b60008060008060008060c087890312156117bc57600080fd5b60006117ca89828a01611604565b965050602087013567ffffffffffffffff8111156117e757600080fd5b6117f389828a016115ae565b955050604087013567ffffffffffffffff81111561181057600080fd5b61181c89828a016115ae565b945050606061182d89828a0161159a565b935050608061183e89828a01611586565b92505060a061184f89828a01611572565b9150509295509295509295565b61186581611d28565b82525050565b600061187682611c9a565b80845260208401935061188883611c59565b60005b828110156118ba5761189e86835161185c565b6118a782611ce7565b915060208601955060018101905061188b565b50849250505092915050565b60006118d182611ca5565b8084526020840193506118e383611c66565b60005b82811015611915576118f9868351611a45565b61190282611cf4565b91506020860195506001810190506118e6565b50849250505092915050565b600061192c82611cb0565b80845260208401935061193e83611c73565b60005b8281101561197057611954868351611a54565b61195d82611d01565b9150602086019550600181019050611941565b50849250505092915050565b600061198782611cbb565b808452602084019350836020820285016119a085611c80565b60005b848110156119d95783830388526119bb838351611a99565b92506119c682611d0e565b91506020880197506001810190506119a3565b508196508694505050505092915050565b60006119f582611cc6565b808452602084019350611a0783611c8d565b60005b82811015611a3957611a1d868351611acf565b611a2682611d1b565b9150602086019550600181019050611a0a565b50849250505092915050565b611a4e81611d48565b82525050565b611a5d81611d54565b82525050565b6000611a6e82611cdc565b808452611a82816020860160208601611db7565b611a8b81611dea565b602085010191505092915050565b6000611aa482611cd1565b808452611ab8816020860160208601611db7565b611ac181611dea565b602085010191505092915050565b611ad881611d5e565b82525050565b600060c0820190508181036000830152611af881896119ea565b90508181036020830152611b0c818861197c565b90508181036040830152611b20818761197c565b90508181036060830152611b348186611921565b90508181036080830152611b4881856118c6565b905081810360a0830152611b5c818461186b565b9050979650505050505050565b6000602082019050611b7e6000830184611a54565b92915050565b600060a0820190508181036000830152611b9e8188611a63565b90508181036020830152611bb28187611a63565b9050611bc16040830186611a54565b611bce606083018561185c565b611bdb6080830184611a45565b9695505050505050565b6000602082019050611bfa6000830184611acf565b92915050565b6000604051905081810181811067ffffffffffffffff82111715611c2357600080fd5b8060405250919050565b600067ffffffffffffffff821115611c4457600080fd5b601f19601f8301169050602081019050919050565b6000602082019050919050565b6000602082019050919050565b6000602082019050919050565b6000602082019050919050565b6000602082019050919050565b600081519050919050565b600081519050919050565b600081519050919050565b600081519050919050565b600081519050919050565b600081519050919050565b600081519050919050565b6000602082019050919050565b6000602082019050919050565b6000602082019050919050565b6000602082019050919050565b6000602082019050919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b60008115159050919050565b6000819050919050565b6000819050919050565b600073ffffffffffffffffffffffffffffffffffffffff82169050919050565b60008115159050919050565b6000819050919050565b6000819050919050565b82818337600083830152505050565b60005b83811015611dd5578082015181840152602081019050611dba565b83811115611de4576000848401525b50505050565b6000601f19601f83011690509190505600a265627a7a723058202e5bec73483d04f429b4cc0624230de5bc933e94dc932eb81a3d0b6dbf9f82f06c6578706572696d656e74616cf50037";

    public static final String ABI = "[{\"constant\":false,\"inputs\":[],\"name\":\"createUser\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"carId\",\"type\":\"uint256\"},{\"name\":\"carNo\",\"type\":\"string\"},{\"name\":\"carModel\",\"type\":\"string\"},{\"name\":\"price\",\"type\":\"int256\"},{\"name\":\"solded\",\"type\":\"bool\"},{\"name\":\"owner_address\",\"type\":\"address\"}],\"name\":\"innerModifyCar\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"carId\",\"type\":\"uint256\"},{\"name\":\"price\",\"type\":\"int256\"}],\"name\":\"reActiveCar\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"getListSize\",\"outputs\":[{\"name\":\"\",\"type\":\"uint256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[{\"name\":\"carId\",\"type\":\"uint256\"}],\"name\":\"getCar\",\"outputs\":[{\"name\":\"carNo\",\"type\":\"string\"},{\"name\":\"carModel\",\"type\":\"string\"},{\"name\":\"price\",\"type\":\"int256\"},{\"name\":\"owner_address\",\"type\":\"address\"},{\"name\":\"solded\",\"type\":\"bool\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"salesCount\",\"outputs\":[{\"name\":\"\",\"type\":\"int256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[],\"name\":\"showBalance\",\"outputs\":[{\"name\":\"\",\"type\":\"int256\"}],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"getCarList\",\"outputs\":[{\"name\":\"carId\",\"type\":\"uint256[]\"},{\"name\":\"carNo\",\"type\":\"string[]\"},{\"name\":\"carModel\",\"type\":\"string[]\"},{\"name\":\"price\",\"type\":\"int256[]\"},{\"name\":\"solded\",\"type\":\"bool[]\"},{\"name\":\"owner_address\",\"type\":\"address[]\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"avgPrice\",\"outputs\":[{\"name\":\"\",\"type\":\"int256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"carId\",\"type\":\"uint256\"},{\"name\":\"carNo\",\"type\":\"string\"},{\"name\":\"carModel\",\"type\":\"string\"},{\"name\":\"price\",\"type\":\"int256\"},{\"name\":\"solded\",\"type\":\"bool\"}],\"name\":\"outerModifyCar\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"getSalesCount\",\"outputs\":[{\"name\":\"\",\"type\":\"int256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"carId\",\"type\":\"uint256\"}],\"name\":\"purchaseCar\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":false,\"inputs\":[{\"name\":\"carNo\",\"type\":\"string\"},{\"name\":\"carModel\",\"type\":\"string\"},{\"name\":\"price\",\"type\":\"int256\"}],\"name\":\"insertCar\",\"outputs\":[],\"payable\":false,\"stateMutability\":\"nonpayable\",\"type\":\"function\"},{\"constant\":true,\"inputs\":[],\"name\":\"getTotalPrice\",\"outputs\":[{\"name\":\"\",\"type\":\"int256\"}],\"payable\":false,\"stateMutability\":\"view\",\"type\":\"function\"}]";

    public static final String FUNC_CREATEUSER = "createUser";

    public static final String FUNC_INNERMODIFYCAR = "innerModifyCar";

    public static final String FUNC_REACTIVECAR = "reActiveCar";

    public static final String FUNC_GETLISTSIZE = "getListSize";

    public static final String FUNC_GETCAR = "getCar";

    public static final String FUNC_SALESCOUNT = "salesCount";

    public static final String FUNC_SHOWBALANCE = "showBalance";

    public static final String FUNC_GETCARLIST = "getCarList";

    public static final String FUNC_AVGPRICE = "avgPrice";

    public static final String FUNC_OUTERMODIFYCAR = "outerModifyCar";

    public static final String FUNC_GETSALESCOUNT = "getSalesCount";

    public static final String FUNC_PURCHASECAR = "purchaseCar";

    public static final String FUNC_INSERTCAR = "insertCar";

    public static final String FUNC_GETTOTALPRICE = "getTotalPrice";

    @Deprecated
    protected CarMarket(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected CarMarket(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, credentials, contractGasProvider);
    }

    @Deprecated
    protected CarMarket(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    protected CarMarket(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        super(BINARY, contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public RemoteCall<TransactionReceipt> createUser() {
        final Function function = new Function(
                FUNC_CREATEUSER, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public void createUser(TransactionSucCallback callback) {
        final Function function = new Function(
                FUNC_CREATEUSER, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        asyncExecuteTransaction(function, callback);
    }

    public String createUserSeq() {
        final Function function = new Function(
                FUNC_CREATEUSER, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return createTransactionSeq(function);
    }

    public RemoteCall<TransactionReceipt> innerModifyCar(BigInteger carId, String carNo, String carModel, BigInteger price, Boolean solded, String owner_address) {
        final Function function = new Function(
                FUNC_INNERMODIFYCAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.generated.Uint256(carId), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carNo), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carModel), 
                new org.fisco.bcos.web3j.abi.datatypes.generated.Int256(price), 
                new org.fisco.bcos.web3j.abi.datatypes.Bool(solded), 
                new org.fisco.bcos.web3j.abi.datatypes.Address(owner_address)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public void innerModifyCar(BigInteger carId, String carNo, String carModel, BigInteger price, Boolean solded, String owner_address, TransactionSucCallback callback) {
        final Function function = new Function(
                FUNC_INNERMODIFYCAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.generated.Uint256(carId), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carNo), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carModel), 
                new org.fisco.bcos.web3j.abi.datatypes.generated.Int256(price), 
                new org.fisco.bcos.web3j.abi.datatypes.Bool(solded), 
                new org.fisco.bcos.web3j.abi.datatypes.Address(owner_address)), 
                Collections.<TypeReference<?>>emptyList());
        asyncExecuteTransaction(function, callback);
    }

    public String innerModifyCarSeq(BigInteger carId, String carNo, String carModel, BigInteger price, Boolean solded, String owner_address) {
        final Function function = new Function(
                FUNC_INNERMODIFYCAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.generated.Uint256(carId), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carNo), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carModel), 
                new org.fisco.bcos.web3j.abi.datatypes.generated.Int256(price), 
                new org.fisco.bcos.web3j.abi.datatypes.Bool(solded), 
                new org.fisco.bcos.web3j.abi.datatypes.Address(owner_address)), 
                Collections.<TypeReference<?>>emptyList());
        return createTransactionSeq(function);
    }

    public RemoteCall<TransactionReceipt> reActiveCar(BigInteger carId, BigInteger price) {
        final Function function = new Function(
                FUNC_REACTIVECAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.generated.Uint256(carId), 
                new org.fisco.bcos.web3j.abi.datatypes.generated.Int256(price)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public void reActiveCar(BigInteger carId, BigInteger price, TransactionSucCallback callback) {
        final Function function = new Function(
                FUNC_REACTIVECAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.generated.Uint256(carId), 
                new org.fisco.bcos.web3j.abi.datatypes.generated.Int256(price)), 
                Collections.<TypeReference<?>>emptyList());
        asyncExecuteTransaction(function, callback);
    }

    public String reActiveCarSeq(BigInteger carId, BigInteger price) {
        final Function function = new Function(
                FUNC_REACTIVECAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.generated.Uint256(carId), 
                new org.fisco.bcos.web3j.abi.datatypes.generated.Int256(price)), 
                Collections.<TypeReference<?>>emptyList());
        return createTransactionSeq(function);
    }

    public RemoteCall<BigInteger> getListSize() {
        final Function function = new Function(FUNC_GETLISTSIZE, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<Tuple5<String, String, BigInteger, String, Boolean>> getCar(BigInteger carId) {
        final Function function = new Function(FUNC_GETCAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.generated.Uint256(carId)), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Utf8String>() {}, new TypeReference<Utf8String>() {}, new TypeReference<Int256>() {}, new TypeReference<Address>() {}, new TypeReference<Bool>() {}));
        return new RemoteCall<Tuple5<String, String, BigInteger, String, Boolean>>(
                new Callable<Tuple5<String, String, BigInteger, String, Boolean>>() {
                    @Override
                    public Tuple5<String, String, BigInteger, String, Boolean> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);
                        return new Tuple5<String, String, BigInteger, String, Boolean>(
                                (String) results.get(0).getValue(), 
                                (String) results.get(1).getValue(), 
                                (BigInteger) results.get(2).getValue(), 
                                (String) results.get(3).getValue(), 
                                (Boolean) results.get(4).getValue());
                    }
                });
    }

    public RemoteCall<BigInteger> salesCount() {
        final Function function = new Function(FUNC_SALESCOUNT, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<TransactionReceipt> showBalance() {
        final Function function = new Function(
                FUNC_SHOWBALANCE, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public void showBalance(TransactionSucCallback callback) {
        final Function function = new Function(
                FUNC_SHOWBALANCE, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        asyncExecuteTransaction(function, callback);
    }

    public String showBalanceSeq() {
        final Function function = new Function(
                FUNC_SHOWBALANCE, 
                Arrays.<Type>asList(), 
                Collections.<TypeReference<?>>emptyList());
        return createTransactionSeq(function);
    }

    public RemoteCall<Tuple6<List<BigInteger>, List<String>, List<String>, List<BigInteger>, List<Boolean>, List<String>>> getCarList() {
        final Function function = new Function(FUNC_GETCARLIST, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<DynamicArray<Uint256>>() {}, new TypeReference<DynamicArray<Utf8String>>() {}, new TypeReference<DynamicArray<Utf8String>>() {}, new TypeReference<DynamicArray<Int256>>() {}, new TypeReference<DynamicArray<Bool>>() {}, new TypeReference<DynamicArray<Address>>() {}));
        return new RemoteCall<Tuple6<List<BigInteger>, List<String>, List<String>, List<BigInteger>, List<Boolean>, List<String>>>(
                new Callable<Tuple6<List<BigInteger>, List<String>, List<String>, List<BigInteger>, List<Boolean>, List<String>>>() {
                    @Override
                    public Tuple6<List<BigInteger>, List<String>, List<String>, List<BigInteger>, List<Boolean>, List<String>> call() throws Exception {
                        List<Type> results = executeCallMultipleValueReturn(function);
                        return new Tuple6<List<BigInteger>, List<String>, List<String>, List<BigInteger>, List<Boolean>, List<String>>(
                                convertToNative((List<Uint256>) results.get(0).getValue()), 
                                convertToNative((List<Utf8String>) results.get(1).getValue()), 
                                convertToNative((List<Utf8String>) results.get(2).getValue()), 
                                convertToNative((List<Int256>) results.get(3).getValue()), 
                                convertToNative((List<Bool>) results.get(4).getValue()), 
                                convertToNative((List<Address>) results.get(5).getValue()));
                    }
                });
    }

    public RemoteCall<BigInteger> avgPrice() {
        final Function function = new Function(FUNC_AVGPRICE, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<TransactionReceipt> outerModifyCar(BigInteger carId, String carNo, String carModel, BigInteger price, Boolean solded) {
        final Function function = new Function(
                FUNC_OUTERMODIFYCAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.generated.Uint256(carId), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carNo), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carModel), 
                new org.fisco.bcos.web3j.abi.datatypes.generated.Int256(price), 
                new org.fisco.bcos.web3j.abi.datatypes.Bool(solded)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public void outerModifyCar(BigInteger carId, String carNo, String carModel, BigInteger price, Boolean solded, TransactionSucCallback callback) {
        final Function function = new Function(
                FUNC_OUTERMODIFYCAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.generated.Uint256(carId), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carNo), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carModel), 
                new org.fisco.bcos.web3j.abi.datatypes.generated.Int256(price), 
                new org.fisco.bcos.web3j.abi.datatypes.Bool(solded)), 
                Collections.<TypeReference<?>>emptyList());
        asyncExecuteTransaction(function, callback);
    }

    public String outerModifyCarSeq(BigInteger carId, String carNo, String carModel, BigInteger price, Boolean solded) {
        final Function function = new Function(
                FUNC_OUTERMODIFYCAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.generated.Uint256(carId), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carNo), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carModel), 
                new org.fisco.bcos.web3j.abi.datatypes.generated.Int256(price), 
                new org.fisco.bcos.web3j.abi.datatypes.Bool(solded)), 
                Collections.<TypeReference<?>>emptyList());
        return createTransactionSeq(function);
    }

    public RemoteCall<BigInteger> getSalesCount() {
        final Function function = new Function(FUNC_GETSALESCOUNT, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    public RemoteCall<TransactionReceipt> purchaseCar(BigInteger carId) {
        final Function function = new Function(
                FUNC_PURCHASECAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.generated.Uint256(carId)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public void purchaseCar(BigInteger carId, TransactionSucCallback callback) {
        final Function function = new Function(
                FUNC_PURCHASECAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.generated.Uint256(carId)), 
                Collections.<TypeReference<?>>emptyList());
        asyncExecuteTransaction(function, callback);
    }

    public String purchaseCarSeq(BigInteger carId) {
        final Function function = new Function(
                FUNC_PURCHASECAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.generated.Uint256(carId)), 
                Collections.<TypeReference<?>>emptyList());
        return createTransactionSeq(function);
    }

    public RemoteCall<TransactionReceipt> insertCar(String carNo, String carModel, BigInteger price) {
        final Function function = new Function(
                FUNC_INSERTCAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carNo), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carModel), 
                new org.fisco.bcos.web3j.abi.datatypes.generated.Int256(price)), 
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public void insertCar(String carNo, String carModel, BigInteger price, TransactionSucCallback callback) {
        final Function function = new Function(
                FUNC_INSERTCAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carNo), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carModel), 
                new org.fisco.bcos.web3j.abi.datatypes.generated.Int256(price)), 
                Collections.<TypeReference<?>>emptyList());
        asyncExecuteTransaction(function, callback);
    }

    public String insertCarSeq(String carNo, String carModel, BigInteger price) {
        final Function function = new Function(
                FUNC_INSERTCAR, 
                Arrays.<Type>asList(new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carNo), 
                new org.fisco.bcos.web3j.abi.datatypes.Utf8String(carModel), 
                new org.fisco.bcos.web3j.abi.datatypes.generated.Int256(price)), 
                Collections.<TypeReference<?>>emptyList());
        return createTransactionSeq(function);
    }

    public RemoteCall<BigInteger> getTotalPrice() {
        final Function function = new Function(FUNC_GETTOTALPRICE, 
                Arrays.<Type>asList(), 
                Arrays.<TypeReference<?>>asList(new TypeReference<Int256>() {}));
        return executeRemoteCallSingleValueReturn(function, BigInteger.class);
    }

    @Deprecated
    public static CarMarket load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new CarMarket(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    @Deprecated
    public static CarMarket load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new CarMarket(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static CarMarket load(String contractAddress, Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return new CarMarket(contractAddress, web3j, credentials, contractGasProvider);
    }

    public static CarMarket load(String contractAddress, Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return new CarMarket(contractAddress, web3j, transactionManager, contractGasProvider);
    }

    public static RemoteCall<CarMarket> deploy(Web3j web3j, Credentials credentials, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(CarMarket.class, web3j, credentials, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<CarMarket> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(CarMarket.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<CarMarket> deploy(Web3j web3j, TransactionManager transactionManager, ContractGasProvider contractGasProvider) {
        return deployRemoteCall(CarMarket.class, web3j, transactionManager, contractGasProvider, BINARY, "");
    }

    @Deprecated
    public static RemoteCall<CarMarket> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(CarMarket.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }
}
