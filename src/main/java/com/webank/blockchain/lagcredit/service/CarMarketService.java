package com.webank.blockchain.lagcredit.service;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.fisco.bcos.web3j.crypto.Credentials;
import org.fisco.bcos.web3j.crypto.ECKeyPair;
import org.fisco.bcos.web3j.crypto.Keys;
import org.fisco.bcos.web3j.crypto.gm.GenCredential;
import org.fisco.bcos.web3j.protocol.Web3j;
import org.fisco.bcos.web3j.protocol.core.methods.response.TransactionReceipt;
import org.fisco.bcos.web3j.tuples.generated.Tuple5;
import org.fisco.bcos.web3j.tuples.generated.Tuple6;
import org.fisco.bcos.web3j.tx.gas.StaticGasProvider;
import org.fisco.bcos.web3j.utils.Numeric;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.webank.blockchain.lagcredit.constants.GasConstants;
import com.webank.blockchain.lagcredit.contracts.CarMarket;

import lombok.extern.slf4j.Slf4j;
import mo.com.carmarket.Car;
import mo.com.carmarket.User;

@Service
@Slf4j
public class CarMarketService {

	static final int PUBLIC_KEY_SIZE = 64;
	static final int PUBLIC_KEY_LENGTH_IN_HEX = PUBLIC_KEY_SIZE << 1;

	@Autowired
	private Web3j web3j;
	@Autowired
	private Credentials credentials;

	public CarMarket deploy() {
		CarMarket carMarket = null;
		try {
			carMarket = CarMarket
					.deploy(web3j, credentials, new StaticGasProvider(GasConstants.GAS_PRICE, GasConstants.GAS_LIMIT))
					.send();
			 log.info("CarMarket address is {}", carMarket.getContractAddress());
			return carMarket;
		} catch (Exception e) {
			 log.error("deploy car-market contract fail: {}", e.getMessage());
		}
		return carMarket;
	}

	public CarMarket load(String carMarketAddress) {
		CarMarket carMarket = CarMarket.load(carMarketAddress, web3j, credentials,
				new StaticGasProvider(GasConstants.GAS_PRICE, GasConstants.GAS_LIMIT));
		return carMarket;
	}

	public CarMarket load(String carMarketAddress, Credentials userCred) {
		CarMarket carMarket = CarMarket.load(carMarketAddress, web3j, userCred,
				new StaticGasProvider(GasConstants.GAS_PRICE, GasConstants.GAS_LIMIT));
		return carMarket;
	}

	public Credentials userCredentials(String privateKey) throws Exception {
		Credentials credentials = GenCredential.create(privateKey);
		if (credentials == null) {
			throw new Exception("create Credentials failed");
		}
		return credentials;
	}

	public List<Car> getCarList(String carMarketAddress) throws Exception {

		List<Car> list = new ArrayList<Car>();
		CarMarket carMarket = load(carMarketAddress);

		Tuple6<List<BigInteger>, List<String>, List<String>, List<BigInteger>, List<Boolean>, List<String>> send = carMarket
				.getCarList().send();

		List<BigInteger> idList = send.getValue1();
		List<String> carNoList = send.getValue2();
		List<String> modelList = send.getValue3();
		List<BigInteger> priceList = send.getValue4();
		List<Boolean> soldList = send.getValue5();
		List<String> ownerAddressList = send.getValue6();
		int count = idList.size();
		for (int i = 0; i < count; i++) {
			Car car = new Car();
			car.setId(idList.get(i).longValue());
			car.setCarNo(carNoList.get(i));
			car.setModel(modelList.get(i));
			car.setPrice(priceList.get(i).intValue());
			car.setSold(soldList.get(i).booleanValue());
			car.setOwnerAddress(ownerAddressList.get(i));
			list.add(car);
		}
		return list;
	}

	public boolean insertCar(String carMarketAddress, User seller, String carNo, String carModel, int price)
			throws Exception {
		CarMarket carMarket = CarMarket.load(carMarketAddress, web3j, userCredentials(seller.getPrivateKey()),
				new StaticGasProvider(GasConstants.GAS_PRICE, GasConstants.GAS_LIMIT));

		TransactionReceipt receipt = carMarket.insertCar(carNo, carModel, BigInteger.valueOf(price)).send();
		/*
		 * try { TransactionReceipt receipt = carMarket.insertCar(carNo, carModel,
		 * BigInteger.valueOf(price)).send(); //log.info("status : {}",
		 * receipt.getStatus()); } catch (Exception e) {
		 * //log.error("insert car fail: {}", e.getMessage()); return false; }
		 */
		return true;
	}

	public long getListSize(String carMarketAddress) throws Exception {
		CarMarket carMarket = load(carMarketAddress);
		try {
			BigInteger size = carMarket.getListSize().send();
			return size.longValue();
		} catch (IndexOutOfBoundsException e) {
			return 0;
		}
	}

	public Car getCar(String carMarketAddress, int carId) throws Exception {
		CarMarket carMarket = load(carMarketAddress);
		Tuple5<String, String, BigInteger, String, Boolean> data = carMarket.getCar(BigInteger.valueOf(carId)).send();

		Car car = new Car();
		car.setId(carId);
		car.setCarNo(data.getValue1());
		car.setModel(data.getValue2());
		car.setPrice(data.getValue3().intValue());
		car.setOwnerAddress(data.getValue4());
		car.setSold(data.getValue5());
		return car;
	}

	public User createUser(String carMarketAddress) throws Exception {
		ECKeyPair keyPair = Keys.createEcKeyPair();
		String publicKey = Numeric.toHexStringWithPrefixZeroPadded(keyPair.getPublicKey(), PUBLIC_KEY_LENGTH_IN_HEX);
		String privateKey = Numeric.toHexStringNoPrefix(keyPair.getPrivateKey());
		String address = "0x" + Keys.getAddress(publicKey);

		User user = new User(address, privateKey, publicKey);
		CarMarket carMarket = load(carMarketAddress, userCredentials(user.getPrivateKey()));
		carMarket.createUser().send();
		return user;
	}

	public boolean purchase(String carMarketAddress, User buyer, int carId) throws Exception {
		System.out.println("call purchase service");
		CarMarket carMarket = load(carMarketAddress, userCredentials(buyer.getPrivateKey()));
		carMarket.purchaseCar(BigInteger.valueOf(carId)).send();

		return true;
	}

	public TransactionReceipt showBalance(String carMarketAddress, User user) throws Exception {
		CarMarket carMarket = load(carMarketAddress, userCredentials(user.getPrivateKey()));
		TransactionReceipt send = carMarket.showBalance().send();
		return send;

	}
	
	public int getTotalPrice(String carMarketAddress) throws Exception {
		CarMarket carMarket = load(carMarketAddress);
		return carMarket.getTotalPrice().send().intValue();
	}
	
	public int getSalesCount(String carMarketAddress) throws Exception {
		CarMarket carMarket = load(carMarketAddress);
		return carMarket.getSalesCount().send().intValue();
	}
	
	public int getAvgPrice(String carMarketAddress) throws Exception  {
		CarMarket carMarket = load(carMarketAddress);
		return carMarket.avgPrice().send().intValue();
	}
}
