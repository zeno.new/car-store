package mo.com.carmarket;

public class User {

	String address;
	String privateKey;
	String publicKey;

	public User(String address, String privateKey, String publicKey) {
		this.address = address;
		this.privateKey = privateKey;
		this.publicKey = publicKey;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPrivateKey() {
		return privateKey;
	}

	public void setPrivateKey(String privateKey) {
		this.privateKey = privateKey;
	}
}
